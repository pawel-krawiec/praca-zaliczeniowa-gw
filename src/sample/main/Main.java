package sample.main;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import sample.boardgame.Board;
import sample.control.Controller;
import sample.music.Music;
import sample.variables.InfoGame;

public class Main extends Application {

    private Board board = new Board();
    private Controller controller = new Controller();

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Galaktyczny Wojownik");
        primaryStage.setScene(new Scene(board.createContent()));
        primaryStage.show();
        Music.PLAYMUSIC(InfoGame.MUSIC);

        primaryStage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(!board.isPause())
                controller.keyDownHandler(event, board.getShip());
                }
        });
        primaryStage.getScene().setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(!board.isPause())
                controller.keyUpHandler(event, board.getShip());
            }
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
