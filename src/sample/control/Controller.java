package sample.control;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import sample.objects.Object;

public class Controller {

    public void keyDownHandler(KeyEvent event, Object object) {
        if (event.getCode() == KeyCode.J) {
            object.setLeft(true);
        }
        if (event.getCode() == KeyCode.L) {
            object.setRight(true);
        }
        if (event.getCode() == KeyCode.I) {
            object.accelerate();
        }
        if (event.getCode() == KeyCode.W) {
            object.shoot();
        }
    }

    public void keyUpHandler(KeyEvent event, Object object) {
        if (event.getCode() == KeyCode.I) {
            object.slowDown();
        }
        if (event.getCode() == KeyCode.J) {
            object.setLeft(false);
        }
        if (event.getCode() == KeyCode.L) {
            object.setRight(false);
        }
    }
}
