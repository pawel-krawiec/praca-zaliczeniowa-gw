package sample.boardgame;

import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import sample.variables.InfoGame;

public class GamePane {

    public Pane p;

    public GamePane(double width, double height, boolean ifImage) {
        this.p = new Pane();

        p.setPrefSize(width, height);
        if (ifImage) {
            p.setBackground(new Background(new BackgroundImage(new Image(GamePane.class.getResourceAsStream("zSTART.jpg")),
                    BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(InfoGame.BOARD_WIDTH,
                    InfoGame.BOARD_HEIGHT, false, false, false, false))));

        } else {
            p.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        }
    }

    public Pane getPane() {
        return p;
    }
}