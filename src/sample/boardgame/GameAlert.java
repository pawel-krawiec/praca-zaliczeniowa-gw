package sample.boardgame;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import sample.variables.InfoGame;

public class GameAlert {
    public Alert alert;

    public GameAlert() {
        this.alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(InfoGame.ALERT);
        alert.setHeaderText(InfoGame.HEADING);
        alert.setContentText(InfoGame.DESCRIPTIONS);
        Label label = new Label();
        TextArea textArea = new TextArea(InfoGame.INFO);
        textArea.setEditable(false);
        textArea.setMaxHeight(Double.MAX_VALUE);
        VBox.setVgrow(textArea, Priority.ALWAYS);
        VBox expContent = new VBox();
        expContent.getChildren().addAll(label, textArea);
        alert.getDialogPane().setExpandableContent(expContent);
        alert.showAndWait();
    }

    public Alert getAlert() {
        return alert;
    }
}
