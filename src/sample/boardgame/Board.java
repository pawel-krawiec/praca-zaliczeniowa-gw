package sample.boardgame;

import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import sample.objects.Bullet;
import sample.objects.Enemy;
import sample.objects.Object;
import sample.objects.Ship;
import sample.variables.InfoGame;
import java.util.*;
import static java.lang.Thread.sleep;

public class Board {
    private int lives, level, score, points, scoreRiseLevel, clip;
    private double enemySpeed, enemyApearSpeed;
    private boolean isRunning = false, pause;
    private Pane boardPane, buttonPane;
    private Button startButton, exitButton, pauseButton, descriptionButton;
    private Ship ship;
    private VBox vBox;
    Set<Enemy> enemies = new HashSet<>();
    List<Enemy> deadEnemies = new ArrayList<>();
    Set<Bullet> bullets = new HashSet<>();
    List<Bullet> deadBullets = new ArrayList<>();
    Text textLives, textLevel, textScore, textBullets;

    public Parent createContent() {
        createVBox();
        createPanes();
        createWorkingButtons();
        addItems();
        return vBox;
    }

    private void createVBox() {
        vBox = new VBox();
        vBox.setPrefSize(InfoGame.BOARD_WIDTH, InfoGame.BOARD_HEIGHT + InfoGame.BUTTON_BOARD_HEIGHT);
    }

    private void createPanes() {
        boardPane = new GamePane(InfoGame.BOARD_WIDTH, InfoGame.BOARD_HEIGHT, true).getPane();
        buttonPane = new GamePane(InfoGame.BOARD_WIDTH, InfoGame.BUTTON_BOARD_HEIGHT, false).getPane();
    }

    private void createWorkingButtons() {
        createNewButtons();
        assignActionToButtons();
    }

    private void createNewButtons() {
        startButton = new GameButton("Nowa gra", 10).getButton();
        exitButton = new GameButton("Wyjście", 340).getButton();
        pauseButton = new GameButton("Pauza", 120).getButton();
        descriptionButton = new GameButton("Opis Gry", 230).getButton();
    }

    private void assignActionToButtons() {
        startButton.setOnAction(start);
        exitButton.setOnAction(wyjscie);
        pauseButton.setOnAction(pauza);
        descriptionButton.setOnAction(opis);
    }

    private void addItems() {
        vBox.getChildren().addAll(boardPane, buttonPane);
        buttonPane.getChildren().addAll(startButton, exitButton, pauseButton, descriptionButton);
    }

    private EventHandler start = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            if (!isRunning) {
                gameOn();
                textOnBoardSettings();
            }
        }
    };

    private EventHandler pauza = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            pauseOn(timer);
        }
    };

    private EventHandler wyjscie = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.exit(0);
        }
    };

    private EventHandler opis = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            new GameAlert().getAlert();
        }
    };

    public void textOnBoardSettings() {
        textLives = new TextBoard((InfoGame.BOARD_WIDTH - InfoGame.BOARD_WIDTH + 15), (InfoGame.BUTTON_BOARD_HEIGHT + InfoGame.BUTTON_HEIGHT), "Lives: " + lives).getText();
        textScore = new TextBoard(InfoGame.BOARD_WIDTH - InfoGame.BOARD_WIDTH + 15, InfoGame.BUTTON_BOARD_HEIGHT / 2 + InfoGame.BUTTON_HEIGHT / 2, "Score: " + score).getText();
        textLevel = new TextBoard(InfoGame.BOARD_WIDTH - 130, InfoGame.BUTTON_BOARD_HEIGHT / 2 + InfoGame.BUTTON_HEIGHT / 2, "Level: " + level).getText();
        textBullets = new TextBoard(1000, 630, "Bullets: " + clip).getText();
        boardPane.getChildren().addAll(textLives, textScore, textLevel, textBullets);
    }

    public AnimationTimer timer = new AnimationTimer() {
        @Override
        public void handle(long now) {
            if (isRunning) {
                enemiesMoving();
                shipControling();
                bulletsControling();
                killingEnemies();
                dying();
                gameOver();
            }
        }
    };

    public void gameOn() {
        if (!isRunning) {
            clearOldMessages();
            boardPane.setBackground(InfoGame.KOSMOS2);
            isRunning = true;
            ship = new Ship();
            addGameObject(ship, InfoGame.BOARD_WIDTH / 2, InfoGame.BOARD_HEIGHT / 2);
            timer.start();
            gameDrivers();
        }
    }

    public void gameDrivers() {
        enemySpeed = 1;
        lives = 3;
        level = 1;
        points = 50;
        score = 0;
        enemyApearSpeed = 0.995;
        scoreRiseLevel = InfoGame.SCORERISELEVEL;
        clip = 20;
    }

    public void addGameObject(Object object, double x, double y) {
        object.getImageView().setTranslateX(x);
        object.getImageView().setTranslateY(y);
        boardPane.getChildren().add(object.getImageView());
    }

    private void addEnemyToSet(Enemy enemy, double x, double y) {
        if ((ship.getImageView().getTranslateX() > x + InfoGame.SAFETY_ZONE || ship.getImageView().getTranslateX() < x - InfoGame.SAFETY_ZONE) ||
                (ship.getImageView().getTranslateY() > y + InfoGame.SAFETY_ZONE || ship.getImageView().getTranslateY() < y - InfoGame.SAFETY_ZONE)) {
            enemies.add(enemy);
            addGameObject(enemy, x, y);
        }
    }

    private void addBulletToSet(Bullet bullet, double x, double y) {
        if (clip > 0) {
            bullets.add(bullet);
            addGameObject(bullet, x, y);
            shoot();
            refreshingClip();
        }
    }

    public void pauseOn(AnimationTimer animationTimer) {
        if (isRunning) {
            if (!pause) {
                animationTimer.stop();
                flashImageView(InfoGame.PAUSE);
                pause = true;
            } else {
                boardPane.getChildren().remove(InfoGame.PAUSE);
                animationTimer.start();
                pause = false;
            }
        }
    }

    private void shipControling() {
        if (ship.isLeft() == true) {
            ship.rotateLeft();
        }

        if (ship.isRight() == true) {
            ship.rotateRight();
        }

        if (ship.isFire() == true) {
            addBulletToSet(new Bullet(ship.getVelocity().multiply(3.70)),
                    ship.getImageView().getTranslateX() + 20 + ((InfoGame.IMAGE_SIZE/2) * ship.getVelocity().getX()),
                    ship.getImageView().getTranslateY() + 20 + ((InfoGame.IMAGE_SIZE/2) * ship.getVelocity().getY()));
            ship.setFire(false);
        }
        ship.update();
    }

    private void enemiesMoving() {
        if (enemies.size() < 50) {
            if (Math.random() > enemyApearSpeed)
                addEnemyToSet(new Enemy(new Point2D(Enemy.getSign() * Math.random() * enemySpeed, Enemy.getSign() * Math.random() * enemySpeed)),
                        Math.random() * boardPane.getPrefWidth(), Math.random() * boardPane.getPrefHeight());
        }
        clearing(enemies, deadEnemies, boardPane);
    }

    private void bulletsControling() {
        clearing(bullets, deadBullets, boardPane);
    }

    public <T extends Object> void clearing(Set<T> s, List<T> l, Pane p) {
        for (T e : s) {
            e.update();
            if (!e.isAlive()) {
                l.add(e);
                p.getChildren().removeAll(e.getImageView());
            }
        }
        if (!l.isEmpty()) {
            for (T e : l) {
                s.remove(e);
            }
            l.clear();
        }
    }

    public Ship getShip() {
        return ship;
    }

    public boolean isPause() {
        return pause;
    }

    private void killingEnemies() {
        for (Bullet bullet : bullets) {
            for (Enemy enemy : enemies) {
                if (bullet.isColliding(enemy)) {
                    bullet.setAlive(false);
                    enemy.setAlive(false);
                    boardPane.getChildren().removeAll(bullet.getImageView(), enemy.getImageView());
                    refreshingScore();
                    reload();
                    refreshingClip();
                }
            }
        }
    }

    private void dying() {
        for (Enemy enemy : enemies) {
            if (enemy.isColliding(ship)) {
                refreshing();
                clearEnemy(enemies);
                clip = 20;
                refreshingClip();
                break;
            }
        }
    }

    private void clearEnemy(Set<Enemy> hs) {
        for (Enemy e : hs) {
            e.setAlive(false);
        }
    }

    private void refreshing() {
        lives--;
        textLives.setText("Lives: " + lives);
    }

    private void refreshingLevel() {
        if (level < 4) {
            level++;
            textLevel.setText("Level: " + level);
            pointsMultiply();
            enemyAccelerate();
            enemyApearSpeed();
            setLevelBackground(level);
            clearEnemy(enemies);
            holdOn();
            checkClip();
            refreshingClip();
        }
    }

    private void refreshingClip() {
        textBullets.setText("Bullets: " + clip);
    }

    private void shoot() {
        clip--;
    }

    private void reload() {
        clip += 5;
    }

    private void checkClip() {
        clip += 15;
    }

    private void refreshingScore() {
        score += points;
        textScore.setText("Score: " + score);
        isLimit();
    }

    private void isLimit() {
        if (score > scoreRiseLevel * level * level) {
            refreshingLevel();
        }
    }

    private void setLevelBackground(int level) {
        if (level == 2) {
            boardPane.setBackground(InfoGame.KOSMOS1);
        }
        if (level == 3) {
            boardPane.setBackground(InfoGame.KOSMOS3);
        }
        if (level == 4) {
            boardPane.setBackground(InfoGame.KOSMOS4);
        }
    }

    private void enemyAccelerate() {
        enemySpeed += 1.2;
    }

    private void enemyApearSpeed() {
        enemyApearSpeed -= 0.01;
    }

    private void pointsMultiply() {
        points += 50;
    }

    private void holdOn() {
        try {
            sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void gameOver() {
        if (lives == 0) {
            isRunning = false;
            flashImageView(InfoGame.GAMEOVER);
            clearAll();
        }
        if (level == 4 && score > InfoGame.WINTHEGAME) {
            isRunning = false;
            flashImageView(InfoGame.YOUWIN);
            clearAll();
        }
    }

    private void flashImageView(ImageView i) {
        boardPane.getChildren().add(i);
        i.setTranslateX(InfoGame.BOARD_WIDTH / 2 - 240);
        i.setTranslateY(InfoGame.BOARD_HEIGHT / 2 - 120);
    }

    public void clearAll() {
        clearEnemy(enemies);
        bulletsControling();
        ship.setAlive(false);
        boardPane.getChildren().removeAll(textLives, textScore, textLevel, textBullets, ship.getImageView());
    }

    public void clearOldMessages() {
        boardPane.getChildren().remove(InfoGame.GAMEOVER);
        boardPane.getChildren().remove(InfoGame.YOUWIN);
    }
}
