package sample.boardgame;

import javafx.scene.control.Button;
import sample.variables.InfoGame;

public class GameButton {
    public Button button;

    public GameButton(String text, double x) {
        this.button = new Button(text);
        button.setPrefSize(InfoGame.BUTTON_WIDTH, InfoGame.BUTTON_HEIGHT);
        button.setTranslateX(x);
        button.setTranslateY(InfoGame.BUTTON_BOARD_HEIGHT / 2 - InfoGame.BUTTON_HEIGHT / 2);

    }

    public Button getButton() {
        return button;
    }
}

