package sample.boardgame;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class TextBoard {
    public Text text;

    public TextBoard(double x, double y, String t) {
        this.text = new Text(t);
        text.setTranslateX(x);
        text.setTranslateY(y);
        text.setFill(Color.DODGERBLUE);
        text.setFont(Font.font("verdana", FontWeight.EXTRA_BOLD, FontPosture.REGULAR, 25));
    }

    public Text getText() {
        return text;
    }

}