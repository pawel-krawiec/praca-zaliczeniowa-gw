package sample.objects;


import javafx.geometry.Point2D;
import javafx.scene.image.ImageView;
import sample.variables.InfoGame;


public abstract class Object {
    protected ImageView imageView;
    protected boolean thrust;
    protected boolean alive;
    protected boolean fire = false;
    protected boolean left = false;
    protected boolean right = false;
    protected Point2D velocity;
    protected Point2D newVelocity;

    public Object(ImageView imageView, boolean thrust, boolean alive, Point2D velocity) {
        this.imageView = imageView;
        this.thrust = thrust;
        this.alive = alive;
        this.velocity = velocity;
        this.newVelocity = new Point2D(0, 0);
    }

    public void rotateRight() {
        imageView.setRotate(imageView.getRotate() + 3);
        setVelocity(new Point2D(Math.cos(Math.toRadians(getRotate())), Math.sin(Math.toRadians(getRotate()))));
    }

    public void rotateLeft() {
        imageView.setRotate(imageView.getRotate() - 3);
        setVelocity(new Point2D(Math.cos(Math.toRadians(getRotate())), Math.sin(Math.toRadians(getRotate()))));
    }

    public void setNewVelocity(Point2D newVelocity) {
        this.newVelocity = newVelocity;
    }

    public void setVelocity(Point2D velocity) {
        this.velocity = velocity;
    }

    public Point2D getVelocity() {
        return velocity;
    }

    public Point2D getNewVelocity() {
        return newVelocity;
    }

    public double getRotate() {
        return imageView.getRotate();
    }


    public ImageView getImageView() {
        return imageView;
    }

    public boolean isFire() {return fire;}

    public boolean isLeft() {
        return left;
    }

    public boolean isRight() {
        return right;
    }

    public void slowDown() {
        thrust = false;

    }

    public void accelerate() {
        thrust = true;
    }

    public void shoot() {
        fire = true;
    }

    protected double mod(double x, double y) {
        double result = x;
        if (x > y + InfoGame.IMAGE_SIZE) {
            result = -InfoGame.IMAGE_SIZE;
        }
        if (x < -InfoGame.IMAGE_SIZE) {
            result = y + InfoGame.IMAGE_SIZE;
        }
        return result;
    }

    public void update() {
        if ((newVelocity.getX() < InfoGame.SHIP_MAX_SPEED && newVelocity.getY() < InfoGame.SHIP_MAX_SPEED ) &&
                (newVelocity.getX() > -InfoGame.SHIP_MAX_SPEED  && newVelocity.getY() > -InfoGame.SHIP_MAX_SPEED )) {
            if (thrust) {
                imageView.setImage(Ship.imageRun);
                setNewVelocity(new Point2D(newVelocity.getX() + InfoGame.SHIP_ACCELERATION * velocity.getX(), newVelocity.getY() + InfoGame.SHIP_ACCELERATION  * velocity.getY()));
            }
        }
        if (!thrust) {
            imageView.setImage(Ship.imageStop);
            setNewVelocity(new Point2D(newVelocity.getX() * InfoGame.SHIP_BRAKING, newVelocity.getY() * InfoGame.SHIP_BRAKING));

        }
        imageView.setTranslateX(mod(imageView.getTranslateX() + newVelocity.getX(), InfoGame.BOARD_WIDTH));
        imageView.setTranslateY(mod(imageView.getTranslateY() + newVelocity.getY(), InfoGame.BOARD_HEIGHT));
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public boolean isColliding(Object other) {
        if (howFar(other) < 0.8*(imageView.getImage().getHeight() / 2 + other.imageView.getImage().getHeight() / 2)) {
            return true;
        }
        return false;
    }

    public double howFar(Object other) {

        double x1 = imageView.getTranslateX() + imageView.getImage().getHeight() / 2;
        double y1 = imageView.getTranslateY() + imageView.getImage().getHeight() / 2;
        double x2 = other.getImageView().getTranslateX() + other.getImageView().getImage().getHeight() / 2;
        double y2 = other.getImageView().getTranslateY() + other.getImageView().getImage().getHeight() / 2;

        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }


    public void setLeft(boolean left) {
        this.left = left;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public void setFire(boolean fire) {
        this.fire = fire;
    }
}