package sample.objects;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sample.variables.InfoGame;


public class Enemy extends Object {
    public Enemy(Point2D newVelocity) {
        super(new ImageView(new Image(Enemy.class.getResourceAsStream("zAsteroid.png"), 60, 60, false, false)),
                false, true, newVelocity);
        this.newVelocity = newVelocity;
    }

    public void update() {
        setNewVelocity(new Point2D(newVelocity.getX(), newVelocity.getY()));
        imageView.setTranslateX(mod(imageView.getTranslateX() + newVelocity.getX(), InfoGame.BOARD_WIDTH));
        imageView.setTranslateY(mod(imageView.getTranslateY() + newVelocity.getY(), InfoGame.BOARD_HEIGHT));
        imageView.setRotate(imageView.getRotate() + 10);
    }

    public static final double getSign() {
        double sign;
        if (Math.random() > 0.5) sign = 1;
        else sign = -1;
        return sign;
    }

}
