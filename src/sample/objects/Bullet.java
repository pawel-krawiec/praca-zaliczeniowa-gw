package sample.objects;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sample.variables.InfoGame;

public class Bullet extends Object {

    public Bullet( Point2D newVelocity) {
        super(new ImageView(new Image( Bullet.class.getResourceAsStream("zBullet.png"),20,20,false,false)),
                false, true, newVelocity);
        this.newVelocity = newVelocity;
        life.start();
    }


    public void update() {

            setVelocity(new Point2D( newVelocity.getX(), newVelocity.getY()));
            imageView.setTranslateX(mod(imageView.getTranslateX()+ velocity.getX() * InfoGame.BULLET_SPEED + newVelocity.getX(), InfoGame.BOARD_WIDTH) );
            imageView.setTranslateY(mod(imageView.getTranslateY()+ velocity.getY() * InfoGame.BULLET_SPEED + newVelocity.getY(), InfoGame.BOARD_HEIGHT) );

            imageView.setRotate(imageView.getRotate() +20);
        }

    private Thread life = new Thread() {
        @Override
        public void run() {
            try {
                sleep(InfoGame.BULLET_RANGE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            alive = false;
        }
    };
}
