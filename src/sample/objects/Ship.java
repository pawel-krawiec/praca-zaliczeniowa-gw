package sample.objects;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sample.variables.InfoGame;

public class Ship extends Object {
    public static final Image imageStop = new Image(Ship.class.getResourceAsStream("zGWv1.png"), InfoGame.IMAGE_SIZE, InfoGame.IMAGE_SIZE, false, false);
    public static final Image imageRun = new Image(Ship.class.getResourceAsStream("zGWv2.png"), InfoGame.IMAGE_SIZE, InfoGame.IMAGE_SIZE, false, false);

    public Ship() {
        super(new ImageView(new Image(Ship.class.getResourceAsStream("zGWv1.png"), InfoGame.IMAGE_SIZE, InfoGame.IMAGE_SIZE, false, false))
                , false, true, new Point2D(0, 0));
        velocity = new Point2D(Math.cos(Math.toRadians(getRotate())), Math.sin(Math.toRadians(getRotate())));
    }

}

