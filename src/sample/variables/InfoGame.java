package sample.variables;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import sample.music.Music;

import java.io.File;

public class InfoGame {
    public static final double
            BULLET_SPEED = 5,
            SHIP_MAX_SPEED = 15,
            SHIP_ACCELERATION = 0.1,
            SHIP_BRAKING = 0.966,
            IMAGE_SIZE=60;


    public static final long
            BULLET_RANGE = 350;


    public static final double
            BOARD_WIDTH = 1200,
            BOARD_HEIGHT = 650,
            BUTTON_BOARD_HEIGHT = 40,
            BUTTON_WIDTH = 80,
            BUTTON_HEIGHT = 20,
            SAFETY_ZONE = 200;

    public static final int
            WINTHEGAME = 9000,//9000; na prezentacje 2000
            SCORERISELEVEL=400;//400; na prezentacje 100

    public static final String
            ALERT = "Galaktyczny Wojownik - Opis Gry",
            HEADING = "GALAKTYCZNY WOJOWNIK - CZAS OSTATECZNEJ KONFRONTACJI",
            DESCRIPTIONS = "Inforamcje i instrukcje :",
            INFO =
                    "Praca Dyplomowa\n" +
                            "Gra \"Galaktyczny Wojownik\" v 1.1 \n" +
                            "\n" +
                            "Paweł Krawiec\n" +
                            "755/POD/2017\n" +
                            "Wrocławska Wyższa Szkoła Informatyki Stosowanej\n" +
                            "Kierunek: Inżynieria Oprogramowania\n" +
                            "Promotor pracy - Krzysztof Węzowski\n" +
                            "\n" +
                            "INSTRUKCJA I OPIS :\n" +
                            "GALAKTYKA \"BRONKI I KROMKI\" ZOSTAŁA NIKCZEMNIE A NAWET PODLE\n" +
                            "ZAATAKOWANA PRZEZ SIŁY OKRUTNEGO CHOLESTEROLIXA. NIEPRZERWANIE\n" +
                            "W PRZESTRZENI KOSMICZNE MNOŻY SIĘ ŚMIERCIONOŚNA MATERIA FATLIVERY.\n" +
                            "OSTATNIĄ NADZIEJĄ NA ZWYCIĘSTWO DOBRA JEST GALAKTYCZNY WOJOWNIK,\n" +
                            "KTÓRY JAKO JEDYNY DAJE OPÓR ZAGROŻENIU.\n" +
                            "JUŻ CZAS NA OSTATECZNĄ KONFRONTACJĘ!!!\n" +
                            "Sterowanie :\n" +
                            "literka I: PRZÓD\n" +
                            "literka J: LEWO\n" +
                            "literka L: DRUGIE LEWO\n" +
                            "literka W: OSTRZAŁ \n";

    public static final ImageView PAUSE = new ImageView(new Image(InfoGame.class.getResourceAsStream("zPauza.png"), 480, 240, false, false));
    public static final ImageView GAMEOVER = new ImageView(new Image(InfoGame.class.getResourceAsStream("zGameOver.png"), 480, 240, false, false));
    public static final ImageView YOUWIN = new ImageView(new Image(InfoGame.class.getResourceAsStream("zZwyciestwo.png"), 480, 240, false, false));
    public static final File MUSIC = new File("D:/Paweł/03-GalaktycznyWojownik/src/sample/music/WielkiGalaktycznyWojownik.wav");
    //public static final File MUSIC = new File(Music.class.getResource("WielkiGalaktycznyWojownik.wav").getFile());

    public static final Background KOSMOS1 = new Background(new BackgroundImage(new Image(InfoGame.class.getResourceAsStream("zKosmos.jpg")),
            BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BOARD_WIDTH,
            BOARD_HEIGHT, false, false, false, false)));
    public static final Background KOSMOS2 = new Background(new BackgroundImage(new Image(InfoGame.class.getResourceAsStream("zKosmos2.png")),
            BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BOARD_WIDTH,
            BOARD_HEIGHT, false, false, false, false)));
    public static final Background KOSMOS3 = new Background(new BackgroundImage(new Image(InfoGame.class.getResourceAsStream("zKosmos3.png")),
            BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BOARD_WIDTH,
            BOARD_HEIGHT, false, false, false, false)));
    public static final Background KOSMOS4 = new Background(new BackgroundImage(new Image(InfoGame.class.getResourceAsStream("zKosmos4.png")),
            BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BOARD_WIDTH,
            BOARD_HEIGHT, false, false, false, false)));
}
